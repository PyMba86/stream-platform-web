import { action, computed, observable } from 'mobx';
import { Client as RpcClient } from "../client/client";
import { Manager, ManagerInfo, ManagersService } from "../services/ManagersService";


export default class ManagersStore {

    public service: ManagersService;

    @observable
    public managers: Manager[];

    constructor(client: RpcClient) {
        this.service = new ManagersService(client);
        this.managers = [];
    }

    @action
    public async list(): Promise<Manager[]> {
        return this.service.list()
            .then(value => {
                this.managers = value;
                return value;
            })
            .catch(reason => {
                throw reason;
            });
    }

    @action
    public async delete(key: string): Promise<boolean> {
        return this.service.delete(key)
            .then(value => {
                if (value) {
                    this.managers = this.managers.filter(manager => manager.key !== key);
                }
                return value;
            })
            .catch(reason => {
                throw reason;
            });
    }

    @action
    public async add(name: string): Promise<ManagerInfo> {
        return this.service.add(name)
            .then(managerInfo => {
                if (managerInfo) {
                    this.managers.push({key: managerInfo.key, name});
                }
                return managerInfo;
            })
            .catch(reason => {
                throw reason;
            });
    }

    @action
    public async disconnect(key: string): Promise<void> {
        return this.service.disconnect(key)
            .then(value => {
                return value;
            })
            .catch(reason => {
                throw reason;
            });
    }

    @computed
    get getManagers() {
        return this.managers;
    }


}