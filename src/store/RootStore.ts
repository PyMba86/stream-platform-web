import { RouterState, RouterStore } from 'mobx-state-router';
import ManagersStore from "./ManagersStore";

import { routes } from "../routes";

import { Client as RpcClient } from "../client/client";
import ProfileStore from "./ProfileStore";


const notFound = new RouterState('notFound');

export class RootStore {
    public routerStore: RouterStore;
    public managersStore: ManagersStore;
    public profileStore: ProfileStore;

    constructor(client: RpcClient) {
        this.routerStore = new RouterStore(this, routes, notFound);
        this.managersStore = new ManagersStore(client);
        this.profileStore = new ProfileStore(client);
    }
}