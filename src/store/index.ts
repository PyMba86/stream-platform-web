import { RootStore } from "store/RootStore";
import ManagersStore from "./ManagersStore";

export { ManagersStore, RootStore };