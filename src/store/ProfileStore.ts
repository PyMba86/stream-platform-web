import { action, observable } from 'mobx';
import { Client as RpcClient } from "../client/client";
import { ProfileService } from "../services/ProfileService";

export const PROFILE_KEY: string = "profile-key";
export const PROFILE_LOGIN: string = "profile-login";

export default class ProfileStore {

    public service: ProfileService;

    @observable
    public key: string;
    public profile: string;

    constructor(client: RpcClient) {
        this.service = new ProfileService(client);
    }

    public checkAuth(): boolean {
        if (this.key) {
            return true;
        } else {
            const cookieProfileKey = localStorage.getItem(PROFILE_KEY);
            const cookieProfileLogin = localStorage.getItem(PROFILE_LOGIN);
            if (cookieProfileKey && cookieProfileLogin) {
                this.key = cookieProfileKey;
                this.profile = cookieProfileLogin;
                return true;
            }
            return false;
        }
    }

    @action
    public async login(login: string, password: string): Promise<boolean> {
        return this.service.login(login, password)
            .then(key => {
                this.key = key;
                this.profile = login;
                localStorage.setItem(PROFILE_KEY, key);
                localStorage.setItem(PROFILE_LOGIN, login);
                return true;
            })
            .catch(reason => {
                localStorage.removeItem(PROFILE_KEY);
                localStorage.removeItem(PROFILE_LOGIN);
                throw reason;
                return false;
            });
    }

    @action
    public async logout(): Promise<void> {
        return new Promise<void>(resolve => {
            this.key = "";
            localStorage.removeItem(PROFILE_KEY);
            localStorage.removeItem(PROFILE_LOGIN);
            resolve();
        });
    }

    @action
    public async signup(login: string, password: string): Promise<boolean> {
        return this.service.signup(login, password);
    }


}