import { createBrowserHistory } from 'history';
import { HistoryAdapter, RouterView } from 'mobx-state-router';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { observer, Provider } from 'ts-mobx-react';
import { RootStore } from "./store/RootStore";
import * as serviceWorker from './utils/serviceWorker';

import { WithLayout } from "./components/WithLayout/WithLayout";
import { DashboardLayout } from "./layouts/DashboardLayout/DashboardLayout";
import { LoginPage, ManagersPage, NotFoundPage, SignUpPage } from "./pages";


import { Client as RpcClient } from "./client/client";
import { Socket as SocketClient } from "./client/websocket/socket";

import './styles/global.scss';

let urlClient: string = "ws://localhost:8091";

if (process.env.NODE_ENV === 'production') {
    // 8091 - Универсальный порт для менеджера и платформы
    urlClient = ((window.location.protocol === "https:") ? "wss://" : "ws://") + window.location.hostname + ":8091";
}

const socket = new SocketClient(urlClient);

// Create rpc client
const client = new RpcClient(socket);

// Create the rootStore
const rootStore = new RootStore(client);

// Observe history changes
const historyAdapter = new HistoryAdapter(rootStore.routerStore, createBrowserHistory());
historyAdapter.observeRouterStateChanges();

@observer
export class Main extends React.Component {


    public render() {

        const viewMap = {
            'login': <LoginPage/>,
            'signup': <SignUpPage/>,
            'notFound': <NotFoundPage/>,
            'managers': WithLayout(ManagersPage, DashboardLayout)
        };

        return (
            <Provider {...rootStore} rootStore={rootStore} routerStore={rootStore.routerStore}>
                <RouterView routerStore={rootStore.routerStore} viewMap={viewMap}/>
            </Provider>
        );
    }
}

ReactDOM.render(<Main/>, document.getElementById('root') as HTMLElement);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();