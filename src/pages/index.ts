export { NotFoundPage } from './NotFound/NotFound';
export { LoginPage } from './Login/Login';
export { SignUpPage } from './SignUp/SignUp';
export { ManagersPage } from './Managers/Managers';