import { RouterStore } from "mobx-state-router";
import * as React from 'react';
import { inject, observer } from 'ts-mobx-react';
import Card from "../../components/Card/Card";
import Form from "../../components/Form/Form";
import FormLayout from "../../components/FormLayout/FormLayout";
import TextField from "../../components/TextField/TextField";
import ProfileStore from "../../store/ProfileStore";
import styles from './SignUp.scss';

@observer
export class SignUpPage extends React.Component {

    @inject('routerStore')
    public routerStore: RouterStore;

    @inject('profileStore')
    public profileStore: ProfileStore;

    public state = {
        login: '',
        password: ''
    };

    public handleChange = (field: any) => {
        return (value: any) => this.setState({[field]: value});
    };

    public handleSignUp = () => {
        this.profileStore.signup(this.state.login, this.state.password)
            .then(status => {
                if (status) {
                    this.routerStore.goTo("login");
                }
            })
        ;
    };

    public handleLogin = () => {
        this.routerStore.goTo("login");
    };

    public render() {
        const {password, login} = this.state;
        return (
            <div className={styles.Login}>
                <div className={styles.LoginCard}>
                    <Card sectioned={true}
                          title="stream-platform-web"
                          secondaryFooterAction={{content: 'Войти', onAction: this.handleLogin}}
                          primaryFooterAction={{content: 'Зарегистрироваться', onAction: this.handleSignUp}}
                    >
                        <Form onSubmit={this.handleSignUp}>
                            <FormLayout>
                                <TextField
                                    value={login}
                                    onChange={this.handleChange('login')}
                                    label="Логин"
                                    type="text"
                                    helpText={<span>Логин профиля</span>}
                                />
                                <TextField
                                    value={password}
                                    onChange={this.handleChange('password')}
                                    label="Пароль"
                                    type="password"
                                    helpText={<span>Пароль профиля</span>}
                                />
                            </FormLayout>
                        </Form>
                    </Card>
                </div>
            </div>
        );
    }
}
