import * as React from 'react';
import { inject, observer } from 'ts-mobx-react';
import Avatar from "../../components/Avatar/Avatar";
import Card from "../../components/Card/Card";
import Layout from "../../components/Layout/Layout";
import Modal from "../../components/Modal/Modal";
import Page from "../../components/Page/Page";
import { ResourceList } from "../../components/ResourceList/ResourceList";
import TextField from "../../components/TextField/TextField";
import TextStyle from "../../components/TextStyle/TextStyle";
import { Manager } from "../../services/ManagersService";
import ManagersStore from "../../store/ManagersStore";


interface Item extends Manager {
    id: number;
}

interface State {
    activeAddModal: boolean;
    nameAddManager: string;
}

@observer
export class ManagersPage extends React.Component {


    @inject('managersStore')
    public managersStore: ManagersStore;

    public state: State = {
        activeAddModal: false,
        nameAddManager: ''
    };

    public componentDidMount() {
        // Получаем список менеджеров с платформы
        this.managersStore.list().then((value) => {
            console.log("load managers list");
        });
    }

    public handleDelete = (key: string) => {
        // Удаляем менеджера
        this.managersStore.delete(key).then(value =>
            console.log("delete manager " + key)
        );
    };

    public handleConnect = () => {
        // Переход на клиент менеджера
        window.open('http://manager.' + window.location.host);
    };

    public handleDisconnect = (key: string) => {
        // Отключаем менеджера
        this.managersStore.disconnect(key).then(value =>
            console.log("disconnect manager " + key)
        );
    };

    public handleAddClick = () => {
        // Добавление менеджера
        const {nameAddManager} = this.state;
        if (nameAddManager) {
            this.managersStore.add(nameAddManager).then(value => {
                console.log("add manager " + value.key);
                this.toggleAddModal();
            });
        }
    };

    public toggleAddModal = () => {
        this.setState({activeAddModal: !this.state.activeAddModal, nameAddManager: ""});
    };

    public handleChange = (value: string) => {
        this.setState({nameAddManager: value});
    };

    public render() {
        const {activeAddModal, nameAddManager} = this.state;


        const modalAddManagerMarkup = (
            <Modal
                open={activeAddModal}
                large={true}
                onClose={this.toggleAddModal}
                title="Менеджер"
                primaryAction={{
                    content: 'Добавить',
                    onAction: this.handleAddClick,
                }}
            >
                <Modal.Section>
                    <TextField
                        label="Название"
                        value={nameAddManager}
                        onChange={this.handleChange}
                    />
                </Modal.Section>
            </Modal>
        );

        const pageMarkup = (
            <Page
                fullWidth={true}
                title='Менеджеры'
                primaryAction={{content: 'Добавить', onAction: this.toggleAddModal}}
            >
                <Layout>
                    <Layout.Section>
                        <Card>
                            <ResourceList
                                resourceName={{singular: 'manager', plural: 'managers'}}
                                items={this.managersStore.getManagers.map(
                                    (value, index) => {
                                        return {
                                            id: index,
                                            ...value
                                        };
                                    })}
                                renderItem={(item: Item) => {
                                    const media = <Avatar initials={item.name.charAt(0)} size="medium" name={name}/>;
                                    const shortcutActions =
                                        [
                                            {
                                                content: 'Подключиться',
                                                onAction: this.handleConnect
                                            },
                                            {
                                                content: 'Отключить',
                                                onAction: () => this.handleDisconnect(item.key)
                                            },
                                            {
                                                content: 'Удалить',
                                                onAction: () => this.handleDelete(item.key)
                                            },
                                        ];

                                    return (
                                        <ResourceList.Item
                                            id={String(item.id)}
                                            onClick={(id) => {
                                                console.log(id);
                                            }}
                                            media={media}
                                            shortcutActions={shortcutActions}
                                            persistActions={true}
                                            context={{
                                                selectMode: false,
                                                resourceName: {singular: 'manager', plural: 'manager'}
                                            }}
                                        >
                                            <h3>
                                                <TextStyle variation="strong">{item.name}</TextStyle>
                                            </h3>
                                        </ResourceList.Item>
                                    );
                                }}
                            />
                        </Card>

                    </Layout.Section>
                </Layout>
            </Page>
        );

        return (
            <div>
                {modalAddManagerMarkup}
                {pageMarkup}
            </div>


        );
    }
}
