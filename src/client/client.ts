import { ErrorCode, JsonRpcError } from "./rpc/errors";
import { Handler as RpcHandler } from "./rpc/handler";
import { Socket as SocketClient } from "./websocket/socket";

export class Client {

    public methodHandlers: Array<(method: string, params?: any, id?: number | string) => any> = [];

    private rpc: RpcHandler;
    private status: boolean;

    constructor(socket: SocketClient) {
        this.rpc = new RpcHandler();
        this.rpc.sender = (message => socket.send(message));

        socket.onmessage = (event: any) => this.rpc.receive(event.data).catch(console.error);
        socket.onopen = (event: any) => this.status = true;

        this.rpc.methodHandler = (method, params, id) => {
            return this.callMethodHandlers(method, params, id);
        };
    }

    public call(method: string, params?: any, metadata?: any): Promise<any> {
        return new Promise<any>((resolve) => {
            setTimeout(
                (): void => {
                    if (this.status) {
                        resolve(this.rpc.call(method, params, metadata));
                    } else {
                        resolve(this.call(method, params, metadata));
                    }
                }, 3);

        });
    }

    protected callMethodHandlers(method: string, params: any, id: number | string): void {
        for (const handler of this.methodHandlers) {
            try {
                return handler(method, params, id);
            } catch (e) {
                if (!(e instanceof MethodNotFoundError)) {
                    throw e;
                }
            }
        }
        throw new JsonRpcError(ErrorCode.MethodNotFound);
    }
}

export class MethodNotFoundError extends Error {
    constructor(method: string) {
        super(`${method} is not found`);
        this.name = "MethodNotFoundError";
    }
}