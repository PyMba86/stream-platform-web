
export interface SocketOptions {
    connectTimeout: number;
    reconnect: boolean;
    reconnectAttempts: number;
    reconnectDelay: number;
    reconnectDelayExponent: number;
}

export interface StoredEventListener {
    type: string;
    listener: EventListener;
}

export type Protocols = string | string[];

export const defaultOptions: SocketOptions = {
    connectTimeout: 250,
    reconnect: true,
    reconnectAttempts: 10,
    reconnectDelay: 3000,
    reconnectDelayExponent: 1.05,
};