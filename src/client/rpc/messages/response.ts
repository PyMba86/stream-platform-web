

export interface ResponseMessage {
    jsonrpc: string;
    result?: any;
    error?: ResponseError;
    id: number | string;
}

export interface ResponseError {
    code: number;
    message: string;
    data?: any;
}
