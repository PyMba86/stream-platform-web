

export interface RequestMessage {
    jsonrpc: string;
    method: string;
    params?: any;
    id: number | string;
    metadata?: any;
}