import * as React from 'react';

import * as api from 'services/api';

export class ComponentExt<P = {}, S = {}> extends React.Component<P, S> {
   protected readonly api = api;
}

