import { Client as RpcClient } from "../client/client";
import { PROFILE_KEY } from "../store/ProfileStore";

export interface ManagerInfo {
    key: string;
}

export interface Manager extends ManagerInfo {
    name: string;
}

export class ManagersService {

    public client: RpcClient;

    constructor(client: RpcClient) {
        this.client = client;
    }

    public list(): Promise<Manager[]> {
        return this.client.call("managers.list", null, {"profile-key": localStorage.getItem(PROFILE_KEY)});
    }

    public delete(key: string): Promise<boolean> {
        return this.client.call("managers.delete", { "key": key }, {"profile-key": localStorage.getItem(PROFILE_KEY)});
    }

    public add(name: string): Promise<ManagerInfo> {
        return this.client.call("managers.add", { "name": name }, {"profile-key": localStorage.getItem(PROFILE_KEY)});
    }

    public disconnect(key: string): Promise<void> {
        return this.client.call("managers.disconnect", { "key": key }, {"profile-key": localStorage.getItem(PROFILE_KEY)});
    }
}