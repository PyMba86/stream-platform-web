import { Client as RpcClient } from "../client/client";


export class ProfileService {

    public client: RpcClient;

    constructor(client: RpcClient) {
        this.client = client;
    }

    public login(login: string, password: string): Promise<string> {
        return this.client.call("profile.login", { "login": login, "password": password });
    }

    public signup(login: string, password: string): Promise<boolean> {
        return this.client.call("profile.signup", { "login": login, "password": password });
    }

}