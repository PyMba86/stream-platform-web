import reactCompose from '@shopify/react-compose';
import { ReactComponent } from '@shopify/react-utilities/types';
import * as React from 'react';
import { Provider as RefProvider } from '../components/WithRef';

export type ComponentClass = React.ComponentClass<any>;

export type WrappingFunction = (
    Component: ReactComponent<any>,
) => ReactComponent<any>;


export default function compose<Props>(
    ...wrappingFunctions: WrappingFunction[]
) {
    return function wrapComponent<ComposedProps, C>(
        OriginalComponent: ReactComponent<ComposedProps> & C,
    ): ReactComponent<Props> & C {
        const Result = reactCompose(...wrappingFunctions)(
            OriginalComponent,
        ) as ReactComponent<ComposedProps>;
        // @ts-ignore
        return React.forwardRef<Props>(
            (props: Props, ref: React.RefObject<any>) => {
                // @ts-ignore
                const userMenuMarkup = ( <Result  {...props} /> );
                return (
                    <RefProvider value={{forwardedRef: ref}}>
                        {userMenuMarkup}
                    </RefProvider>
                );
            },
        ) as ReactComponent<Props> & C;
    };
}