import React from 'react';
import Card from "../Card";
import { defaultMenuStyle, defaultSetting, defaultWrapperStyle } from './defautSettings';
import { getClientRect, notUndefOrNull, testPassiveEventSupport } from './utils';

export const innerStyle = (translate: number, dragging: boolean, mounted: boolean, transition: number): React.CSSProperties => {
    return {
        width: '9900px',
        transform: `translate3d(${translate}px, 0px, 0px)`,
        transition: `transform ${dragging || !mounted ? '0' : transition}s`,
        whiteSpace: 'nowrap',
        textAlign: 'left',
        userSelect: 'none'
    };
};

export interface InnerProps {
    setRef: any;
    onClick: any;
    translate: number;
    dragging: boolean;
    mounted: boolean;
    transition: number;
    selected?: string | number;
    innerWrapperClass?: string;
    itemClass?: string;
    itemClassActive?: string;
}

export class InnerWrapper extends React.Component<InnerProps> {


    public static defaultProps: InnerProps = {
        translate: defaultSetting.translate,
        dragging: true,
        mounted: false,
        transition: defaultSetting.transition,
        selected: defaultSetting.selected,
        onClick: () => {
            console.log("onClick");
        },
        setRef: () => {
            console.log("setRef");
        }
    };

    public ref: {};

    constructor(props: any) {
        super(props);
        this.ref = {};
    }

    public setRef = (key: any, value: any) => {
        const {setRef} = this.props;
        this.ref[key] = value;
        setRef(this.ref);
    };

    public render() {
        const {
            children,
            translate,
            dragging,
            mounted,
            transition,
            selected,
            innerWrapperClass,
            itemClass,
            onClick,
            itemClassActive
        } = this.props;
        const isActive = (itemId: any, selected: any) => String(itemId) === String(selected);
        if (children == null) { return <div/>; }
        const items = children
            .map(el => React.cloneElement(el, {selected: isActive(el.key, selected)}));
        return (
            <div
                className={innerWrapperClass}
                style={innerStyle(translate, dragging, mounted, transition)}
                ref={inst => this.setRef('menuInner', inst)}
            >
                {items.map((Item, i) => (
                    <div
                        ref={inst => this.setRef(`menuitem-${i}`, inst)}
                        className={`${itemClass} ${isActive(Item.key, selected) ? itemClassActive : ''}`}
                        key={'menuItem-' + Item.key}
                        style={{
                            display: 'inline-block',
                            margin: "6px 12px 6px 6px"
                        }}
                        onClick={() => onClick(Item.key)}
                    >
                        <div
                            className={`menu-item ${selected ? 'active' : ''}`}
                        >
                            <Card title={'Название ' + i} sectioned={true}>
                                <p>{i}</p>
                            </Card>
                        </div>
                    </div>
                ))}
            </div>
        );
    }
}

export interface Props {
    alignCenter?: boolean;
    clickWhenDrag?: boolean;
    data: any[];
    dragging?: boolean;
    innerWrapperClass?: string;
    itemClass?: string;
    itemClassActive?: string;
    hideArrows?: boolean;
    hideSingleArrow?: boolean;
    onSelect?: any;
    onClick?: any;
    selected: number | string;
    translate?: number;
    transition?: number;
    onUpdate?: any;
    menuClass?: string;
    menuStyle?: any;
    wrapperStyle?: any;
    wheel?: boolean;
    wrapperClass?: string;
}

export interface State {
    dragging?: boolean;
    leftArrowVisible?: boolean;
    rightArrowVisible?: boolean;
    xPoint: number;
    translate: any;
    startDragTranslate: any;
    xDraggedDistance: any;
}

export class Carousel extends React.Component<Props, State> {

    public static defaultProps: Props = {transition: 0, data: [], selected: 0};

    private ref: any;
    private mounted: any;
    private needUpdate: any;
    private readonly allItemsWidth: any;
    private readonly menuPos: any;
    private readonly menuWidth: any;
    private readonly wWidth: any;
    private firstPageOffset: any;
    private lastPageOffset: any;
    private selected: any;
    private menuItems: any;

    constructor(props: Props) {
        super(props);
        this.ref = {};
        this.mounted = false;
        this.needUpdate = false;
        this.allItemsWidth = 0;
        this.menuPos = 0;
        this.menuWidth = 0;
        this.wWidth = 0;
        this.firstPageOffset = 0;
        this.lastPageOffset = 0;
    }

    public state: Readonly<State> = {
        dragging: false,
        xPoint: defaultSetting.xPoint,
        translate: this.props.translate,
        startDragTranslate: null,
        xDraggedDistance: null
    };

    public componentDidMount() {
        this.setInitial();

        const passiveEvents = testPassiveEventSupport();
        const optionsCapture = passiveEvents ? {passive: true, capture: true} : true;
        const optionsNoCapture = passiveEvents ? {passive: true, capture: false} : false;

        // if styles loaded before js bundle need wait for it
        window.addEventListener('load', this.onLoad, optionsNoCapture);
        window.addEventListener('resize', this.setInitial, optionsNoCapture);
        document.addEventListener('mousemove', this.handleDrag, optionsCapture);
        document.addEventListener('mouseup', this.handleDragStop, optionsCapture);
        setTimeout(() => this.onLoad(), 0);
    }

    public shouldComponentUpdate(nextProps: any, nextState: any) {
        const {translate, dragging, leftArrowVisible, rightArrowVisible} = this.state;
        const {
            translate: translateNew,
            dragging: draggingNew,
            leftArrowVisible: leftArrowVisibleNew,
            rightArrowVisible: rightArrowVisibleNew
        } = nextState;

        const {
            translate: translateProps,
            selected: selectedProps
        } = this.props;
        const {
            translate: translatePropsNew,
            selected: selectedPropsNew
        } = nextProps;

        const translatePropsDiff = notUndefOrNull(translatePropsNew) &&
            translateProps !== translatePropsNew;
        const selectedPropsDiff = notUndefOrNull(selectedPropsNew) &&
            selectedProps !== selectedPropsNew;
        const propsDiff = translatePropsDiff || selectedPropsDiff;
        const leftArrowVisibleDiff = leftArrowVisible !== leftArrowVisibleNew;
        const rightArrowVisibleDiff = rightArrowVisible !== rightArrowVisibleNew;

        let newMenuItems = false;
        if (this.props.data !== nextProps.data || this.props.data.length !== nextProps.data.length) {
            newMenuItems = true;
            this.needUpdate = true;
        }

        if (propsDiff) {
            if (selectedPropsDiff) {
                this.selected = selectedPropsNew;
            }

            if (!isNaN(translatePropsNew) && translatePropsDiff && !newMenuItems) {
                this.setState({translate: +translatePropsNew.toFixed(3)});
            }
        }

        return (
            newMenuItems ||
            translate !== translateNew ||
            dragging !== draggingNew ||
            propsDiff ||
            leftArrowVisibleDiff ||
            rightArrowVisibleDiff
        );
    }

    public componentDidUpdate() {
        if (this.needUpdate) {
            this.needUpdate = false;
            this.onLoad();
        }
        const {hideSingleArrow, transition = 0} = this.props;
        if (hideSingleArrow) {
            requestAnimationFrame(this.setSingleArrowVisibility);
            setTimeout(() => requestAnimationFrame(this.setSingleArrowVisibility), transition * 1000 + 10);
        }
    }

    public componentWillUnmount() {
        window.removeEventListener('resize', this.setInitial);
        document.removeEventListener('mousemove', this.handleDrag);
        document.removeEventListener('mouseup', this.handleDragStop);
    }

    public setRef = (ref: any) => {
        this.ref = ref;
    };

    public setWrapperRef = (ref: any) => {
        this.ref.menuWrapper = ref;
    };

    public checkSingleArrowVisibility = ({translate = this.state.translate}) => {
        const {hideSingleArrow} = this.props;
        let [leftArrowVisible, rightArrowVisible] = [true, true];
        const {menuItems: items} = this;

        if (hideSingleArrow && items) {
            const visibleItems = this.getVisibleItems({offset: translate});
            const firstItemVisible = visibleItems.includes(items[0]);
            const lastItemVisible = visibleItems.includes(items.slice(-1)[0]);
            leftArrowVisible = !firstItemVisible;
            rightArrowVisible = !lastItemVisible;
        }

        return {leftArrowVisible, rightArrowVisible};
    };

    public setSingleArrowVisibility = () => {
        const {leftArrowVisible, rightArrowVisible} = this.state;
        const {leftArrowVisible: leftArrowVisibleNew, rightArrowVisible: rightArrowVisibleNew} = this.checkSingleArrowVisibility({});
        if (leftArrowVisible !== leftArrowVisibleNew || rightArrowVisible !== rightArrowVisibleNew) {
            this.setState({leftArrowVisible: leftArrowVisibleNew, rightArrowVisible: rightArrowVisibleNew});
        }
    };

    public onLoad = () => {
        this.mounted = true;
        this.setInitial();
    };

    public setInitial = (): void => {
        const {selected, data} = this.props;
        const {translate} = this.state;
        if (!data || !data.length) {
            return;
        }

        const menuItems = this.getMenuItems(data.length);
        const selectedItem = data.find(el => el.key === selected);

        const values = {
            menuItems,
            selected: selectedItem && selectedItem !== -1
                ? selectedItem.key
                : defaultSetting.selected
        };

        for (const key in values) {
            if (values.hasOwnProperty(key)) {
                this[key] = values[key];
            }
        }

        const {translate: _, ...width} = this.updateWidth({items: menuItems, offset: 0, translate: 0});
        for (const key in width) {
            if (width.hasOwnProperty(key)) {
                this[key] = width[key];
            }
        }
        const translateNewRaw = this.getAlignItemsOffset();
        const translateNew = typeof (translateNewRaw) === 'number' ? +translateNewRaw.toFixed(3) : false;

        const {leftArrowVisible, rightArrowVisible} = this.checkSingleArrowVisibility({translate: translateNew});

        if (typeof (translateNew) === 'number' && translate !== translateNew) {
            this.setState(
                {translate: translateNew, leftArrowVisible, rightArrowVisible},
                () => this.onUpdate({translate: translateNew})
            );
        }
    };

    public getMenuItems = (dataLength: any) => {
        return Object.entries(this.ref)
            .filter(el => el[0].includes('menuitem'))
            .slice(0, dataLength)
            .filter(Boolean);
    };

    public getItemsWidth = ({items = this.menuItems}) => {
        const data = items && items.items || items;
        // TODO maybe while cycle faster, need test
        return data.map(el => el[1])
            .filter(Boolean)
            .reduce((acc, el) => acc += getClientRect(el).width, 0);
    };

    public getWidth = (items: any) => {
        const wWidth = window && window.innerWidth;
        const {x: menuPos, width: menuWidth} = getClientRect(this.ref.menuWrapper);
        const allItemsWidth = this.getItemsWidth({items});
        return {wWidth, menuPos, menuWidth, allItemsWidth};
    };

    public updateWidth = ({items = this.menuItems, ...args}) => {
        const {alignCenter} = this.props;
        const width = this.getWidth({items});
        return {...width, ...(alignCenter ? this.getPagesOffsets({items, ...width, ...args}) : {})};
    };

    public getPagesOffsets = ({
                                  items = this.menuItems,
                                  allItemsWidth = this.allItemsWidth,
                                  wWidth = this.wWidth,
                                  menuPos = this.menuPos,
                                  menuWidth = this.menuWidth,
                                  translate = this.state.translate,
                                  offset = this.state.translate
                              }) => {
        const {alignCenter} = this.props;
        const visibleItemsStart = this.getVisibleItems({offset, items, wWidth, menuPos, menuWidth});
        const firstPageOffset = +this.getCenterOffset({items: visibleItemsStart, menuWidth}).toFixed(3);
        const visibleItemsEnd = this.getVisibleItems({
            items,
            offset: -allItemsWidth + menuWidth,
            wWidth,
            menuPos,
            menuWidth
        });
        const lastPageOffset = +this.getCenterOffset({items: visibleItemsEnd, menuWidth}).toFixed(3);
        const trans = translate === 0 && alignCenter ? firstPageOffset : translate;
        this.firstPageOffset = firstPageOffset;
        this.lastPageOffset = lastPageOffset;
        return {firstPageOffset, lastPageOffset, translate: +trans.toFixed(3)};
    };

    public onItemClick = id => {
        const {clickWhenDrag, onSelect} = this.props;
        const {xDraggedDistance} = this.state;

        const afterScroll = xDraggedDistance > 5;

        if (afterScroll && !clickWhenDrag) {
            return false;
        }

        this.selected = id;
        if (onSelect) {
            onSelect(id);
        }
    };

    public getVisibleItems = ({
                                  items = this.menuItems,
                                  wWidth = this.wWidth,
                                  menuPos = this.menuPos,
                                  menuWidth = this.menuWidth,
                                  offset = this.state.translate,
                                  translate = this.state.translate
                              }) => {
        const data = items.items || items;

        return data.filter(el => {
            const {width: elWidth} = getClientRect(el[1]);
            const id = this.getItemInd(items, el);
            const x = this.getOffsetToItem({itemId: id, menuItems: items, translate});

            return this.elemVisible({x, elWidth, wWidth, menuPos, menuWidth, offset});
        });
    };

    public elemVisible = ({
                              x,
                              offset = 0,
                              elWidth,
                              wWidth = this.wWidth,
                              menuPos = this.menuPos,
                              menuWidth = this.menuWidth
                          }) => {
        const leftEdge = menuPos - 1;
        const rightEdge = wWidth - (wWidth - (menuPos + menuWidth)) + 1;
        const pos = x + offset;
        const posWithWidth = pos + elWidth;
        return pos >= leftEdge && posWithWidth <= rightEdge;
    };

    public getItemInd = (menuItems, item) => {
        if (!menuItems || !item) {
            return 0;
        }
        return menuItems.findIndex(el => el[0] === item[0]);
    };

    public getNextItemInd = (left, visibleItems) => {
        const {menuItems} = this;
        if (left) {
            if (!visibleItems.length) {
                return 0;
            }
        } else {
            if (!visibleItems.length) {
                return menuItems.length;
            }
        }
        const ind = left
            ? this.getItemInd(menuItems, visibleItems[0]) - 1
            : this.getItemInd(menuItems, visibleItems.slice(-1)[0]) + 1;
        return ind < 0 ? 0 : ind;
    };

    public getOffsetToItem = ({
                                  itemId,
                                  menuItems = this.menuItems,
                                  translate = this.state.translate
                              }) => {
        if (!menuItems.length) {
            return 0;
        }
        const id = itemId >= menuItems.length ? menuItems.length - 1 : itemId;
        const {x} = getClientRect(menuItems[id][1]);
        const position = +x - translate;
        return position;
    };

    public getScrollRightOffset = (visibleItems, items) => {
        const {alignCenter} = this.props;
        const {menuPos, lastPageOffset} = this;

        const nextItem = this.getNextItem(
            visibleItems && visibleItems.slice(-1)[0]
                ? visibleItems.slice(-1)[0][0]
                : items.slice(-1)[0][0]
        );
        const nextItemIndex = items.findIndex(el => el[0] === nextItem[0]);

        const offsetToItem = this.getOffsetToItem({itemId: nextItemIndex, menuItems: items});
        const offsetToItemOnStart = offsetToItem - menuPos;

        const nextVisibleItems = this.getVisibleItems({
            items,
            offset: -offsetToItemOnStart
        });

        if (nextVisibleItems.includes(items.slice(-1)[0])) {
            return alignCenter ? offsetToItemOnStart + lastPageOffset : offsetToItemOnStart;
        }

        const centerOffset = () => this.getCenterOffset({items: nextVisibleItems});

        const newOffset = alignCenter
            ? offsetToItemOnStart - centerOffset()
            : offsetToItemOnStart;
        return newOffset;
    };

    public getScrollLeftOffset = (visibleItems, items) => {
        const {alignCenter} = this.props;
        const {menuPos, menuWidth, firstPageOffset} = this;

        const prevItem = this.getPrevItem(
            visibleItems && visibleItems[0]
                ? visibleItems[0][0]
                : items[0][0]
        );
        const prevItemIndex = items.findIndex(el => el[0] === prevItem[0]);

        const offsetToItem = this.getOffsetToItem({itemId: prevItemIndex, menuItems: items});
        const itemWidth = this.getItemsWidth({items: [prevItem]});
        const offsetToItemOnEnd = offsetToItem - menuPos - (menuWidth - itemWidth);

        const nextVisibleItems = this.getVisibleItems({
            items,
            offset: -offsetToItemOnEnd
        });

        if (nextVisibleItems.includes(items[0])) {
            return alignCenter ? -firstPageOffset : 0;
        }

        const centerOffset = () => this.getCenterOffset({items: nextVisibleItems});

        const newOffset = alignCenter
            ? offsetToItemOnEnd + centerOffset()
            : offsetToItemOnEnd;
        return newOffset;
    };

    public getAlignItemsOffset = () => {
        const {menuWidth, allItemsWidth, menuItems, firstPageOffset, lastPageOffset} = this;
        const {alignCenter} = this.props;
        const {translate} = this.state;

        if (allItemsWidth < menuWidth) {
            return this.handleArrowClick(!alignCenter);
        }

        const visibleItems = (this.getVisibleItems({}) || []);
        const left = visibleItems.includes(menuItems[0]);
        const right = visibleItems.includes(menuItems.slice(-1)[0]);

        // center is visible, do nothing
        if (!left && !right) {
          //  return +translate.toFixed(3);
        }

        // left edge visible
        if (left) {
            const transl = alignCenter ? firstPageOffset : defaultSetting.translate;
            return +transl.toFixed(3);
        } else {
            const offset = allItemsWidth - menuWidth;
            const transl = alignCenter ? -offset - lastPageOffset : -offset;
            return +transl.toFixed(3);
        }
    };

    public getNextItem = key => {
        const {menuItems} = this;
        const itemIndex = menuItems.findIndex(el => el[0] === key);
        const nextItemIndex = itemIndex + 1;
        const nextItem = menuItems[nextItemIndex] || menuItems.slice(-1)[0];
        return nextItem;
    };

    public getPrevItem = key => {
        const {menuItems} = this;
        const itemIndex = menuItems.findIndex(el => el[0] === key);
        const prevItemIndex = itemIndex - 1;
        const prevItem = menuItems[prevItemIndex] || menuItems[0];
        return prevItem;
    };

    public getOffset = left => {
        const {menuItems: items} = this;
        const visibleItems = this.getVisibleItems({items});
        const newOffset = left
            ? this.getScrollLeftOffset(visibleItems, items)
            : this.getScrollRightOffset(visibleItems, items);
        return newOffset;
    };

    public getCenterOffset = ({
                                  items = this.menuItems,
                                  menuWidth = this.menuWidth
                              }) => {
        if (!items.length) {
            return 0;
        }
        const itemsWidth = this.getItemsWidth({items});
        const offset = (menuWidth - itemsWidth) / 2;
        return +offset.toFixed(3);
    };

    public handleWheel = (e: any): void => {
        const {wheel} = this.props;
        if (!wheel) {
            return;
        }
        if (e.deltaY < 0) {
            this.handleArrowClick();
        } else {
            this.handleArrowClick(false);
        }
    };

    public handleArrowClickRight = () => {
        this.handleArrowClick(false);
    };

    public handleArrowClick = (left = true): void => {
        const {alignCenter} = this.props;
        const {
            allItemsWidth,
            menuWidth,
            firstPageOffset,
            lastPageOffset
        } = this;
        const {translate} = this.state;

        if (!alignCenter && !left && menuWidth > allItemsWidth) {
            return;
        }

        const offset = this.getOffset(left);
        let transl = -offset;

        if (left && this.itBeforeStart(transl)) {
            transl = alignCenter ? firstPageOffset : defaultSetting.translate;
        }
        if (!left && this.itAfterEnd(transl)) {
            const offset = allItemsWidth - menuWidth;
            transl = alignCenter ? -offset - lastPageOffset : -offset;
        }

        const newTranslate = +transl.toFixed(3);

        this.setState(
            {
                translate: newTranslate,
                xPoint: defaultSetting.xPoint,
                startDragTranslate: null,
                xDraggedDistance: null
            },
            () => {
                if (translate !== newTranslate) {
                    this.onUpdate({});
                }
            }
        );
    };

    public itBeforeStart = trans => {
        const {alignCenter} = this.props;
        const {firstPageOffset} = this;
        return alignCenter
            ? trans > firstPageOffset
            : trans > defaultSetting.translate;
    };
    public itAfterEnd = trans => {
        const {alignCenter} = this.props;
        const {menuWidth, allItemsWidth, lastPageOffset} = this;
        return alignCenter
            ? trans < defaultSetting.translate && Math.abs(trans) > allItemsWidth - menuWidth + lastPageOffset
            : trans < defaultSetting.translate && Math.abs(trans) > allItemsWidth - menuWidth;
    };

    public getPoint = (e: any): boolean => {
        return e.touches && e.touches.length
            ? e.touches[0].clientX
            : e.clientX;
    };

    public handleDragStart = (ev: any): void => {
        // 1 left button, 2 right button
        if (ev && ev.buttons === 2) {
            return;
        }
        const {dragging: draggingEnable} = this.props;
        if (!draggingEnable) {
            return;
        }
        const {translate: startDragTranslate} = this.state;
        this.setState({dragging: true, xPoint: 0, startDragTranslate, xDraggedDistance: 0});
    };

    public handleDrag = (e: any): void => {
        const {dragging: draggingEnable} = this.props;
        const {translate, dragging, xPoint, xDraggedDistance} = this.state;
        if (!draggingEnable || !dragging) {
            return;
        }

        const point = this.getPoint(e);
        const diff = xPoint === defaultSetting.xPoint ? defaultSetting.xPoint : xPoint - point;
        let result = translate - diff;

        // don't let scroll over start and end
        if (this.itBeforeStart(result)) {
            result = result - Math.abs(diff) / 2;
        }
        if (this.itAfterEnd(result)) {
            result = result + Math.abs(diff) / 2;
        }

        const newTranslate = +result.toFixed(3);

        this.setState(
            {
                xPoint: point,
                translate: newTranslate,
                xDraggedDistance: xDraggedDistance + Math.abs(diff)
            }
        );
    };

    public handleDragStop = (e: any): void => {
        const {
            allItemsWidth,
            menuWidth,
            firstPageOffset,
            lastPageOffset,
            startDragTranslate
        } = this;
        let {
            xPoint = this.getPoint(e),
        } = this.state;
        const {
            dragging,
            translate,
        } = this.state;
        const {dragging: draggingEnable, alignCenter} = this.props;
        if (!draggingEnable || !dragging) {
            return;
        }

        let newTranslate = translate;

        if (this.itBeforeStart(translate)) {
            newTranslate = alignCenter ? firstPageOffset : defaultSetting.translate;
            xPoint = defaultSetting.xPoint;
        }
        if (this.itAfterEnd(translate)) {
            const offset = allItemsWidth - menuWidth;
            newTranslate = alignCenter ? -offset - lastPageOffset : -offset;
            xPoint = defaultSetting.xPoint;
        }

        if (!alignCenter && menuWidth >= allItemsWidth) {
            newTranslate = defaultSetting.translate;
            xPoint = defaultSetting.xPoint;
        }

        this.setState(
            {
                dragging: false,
                xPoint,
                translate: +newTranslate.toFixed(3)
            },
            () => {
                if (startDragTranslate !== newTranslate) {
                    this.onUpdate({});
                }
            }
        );
    };

    public isArrowsVisible = () => {
        const {allItemsWidth, menuWidth, props: {hideArrows}} = this;
        const hide = Boolean(hideArrows && allItemsWidth <= menuWidth);
        return !hide;
    };

    public onUpdate = ({translate = this.state.translate}) => {
        const {onUpdate} = this.props;
        if (onUpdate) {
            onUpdate({translate});
        }
    };

    public render() {
        const {
            data,
            innerWrapperClass,
            itemClass,
            itemClassActive,
            menuStyle,
            menuClass,
            transition,
            wrapperClass,
            wrapperStyle
        } = this.props;
        const {translate, dragging} = this.state;
        const {selected, mounted} = this;

        if (!data || !data.length) {
            return null;
        }

        const menuStyles = {...defaultMenuStyle, ...menuStyle};
        const wrapperStyles = {...defaultWrapperStyle, ...wrapperStyle};

        return (
            <div
                className={menuClass}
                style={menuStyles}
                onWheel={this.handleWheel}
            >
                <div
                    className={wrapperClass}
                    style={wrapperStyles}
                    ref={this.setWrapperRef}
                    onMouseDown={this.handleDragStart}
                    onTouchStart={this.handleDragStart}
                    onTouchEnd={this.handleDragStop}
                    onMouseMove={this.handleDrag}
                    onTouchMove={this.handleDrag}
                >

                    <InnerWrapper
                        translate={translate}
                        dragging={dragging}
                        mounted={mounted}
                        transition={transition}
                        selected={selected}
                        setRef={this.setRef}
                        onClick={this.onItemClick}
                        innerWrapperClass={innerWrapperClass}
                        itemClass={itemClass}
                        itemClassActive={itemClassActive}
                    >
                        {data}
                    </InnerWrapper>
                </div>
            </div>
        );
    }
}

export default Carousel;