const notUndefOrNull = (val: any) => val !== undefined && val !== null;
const getClientRect = (elem: any) => {
    const { x, left, width } = elem.getBoundingClientRect();
    return { width, x: !isNaN(x) ? +x : +left };
};

const testPassiveEventSupport = () => {
    let passiveSupported = false;

    try {
        const options: any = {
            get passive() { // This function will be called when the browser
                // attempts to access the passive property.
                passiveSupported = true;
                return false;
            }
        };

        window.addEventListener('testPassiveEventSupport', options, options);
        window.removeEventListener('testPassiveEventSupport', options, options);
    } catch (err) {
        passiveSupported = false;
    }
    return passiveSupported;
};

export { notUndefOrNull, getClientRect, testPassiveEventSupport };