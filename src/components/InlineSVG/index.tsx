import React from 'react';

const getRequestsByUrl = {};
const loadedIcons = {};


export interface Props {
    children?: React.ReactNode;
    src: string;
}

export interface State {
    loadedText: string;
}

export default class InlineSVG extends React.PureComponent<Props, State> {

    private isActive: boolean;

    public componentWillMount() {
        this.isActive = true;
    }


    public componentWillUnmount() {
        this.isActive = false;
    }

    public getFile(callback: any) {
        const {src} = this.props;

        if (loadedIcons[src]) {
            const [err, res] = loadedIcons[src];
            callback(err, res, true);
        }

        if (!getRequestsByUrl[src]) {
            getRequestsByUrl[src] = [];
        }
    }

    public load() {
        const {src} = this.props;
        const match = src.match(/data:image\/svg[^,]*?(;base64)?,(.*)/);

        if (match) {
            return this.handleLoad(
                match[1] ? atob(match[2]) : decodeURIComponent(match[2])
            );
        }

        return this.getFile(this.handleLoad);
    }

    public handleLoad = (res: string) => {
        if (this.isActive) {
            this.setState({
                loadedText: res,
            });
        }
    };

    public render() {
        const {loadedText} = this.state;
        let html;

        if (loadedText) {
            html = {
                __html: loadedText
            };
        }

        return React.createFactory('svg')({
            dangerouslySetInnerHTML: html,
        });
    }
}