import { autobind } from '@shopify/javascript-utilities/decorators';
import { write } from '@shopify/javascript-utilities/fastdom';
import { focusFirstFocusableNode } from '@shopify/javascript-utilities/focus';
import { createUniqueIDFactory } from '@shopify/javascript-utilities/other';
import { wrapWithComponent } from '@shopify/react-utilities';
import * as React from 'react';
import { TransitionGroup } from 'react-transition-group';

import { contentContextTypes } from '../../types';

import Portal from '../Portal';
import Scrollable from '../Scrollable';
import Spinner from '../Spinner';

import {
    CloseButton,
    Dialog,
    Footer,
    FooterProps,
    Header,
    Section,
} from './components';
import styles from './Modal.scss';

const IFRAME_LOADING_HEIGHT = 200;

export type Size = 'Small' | 'Medium' | 'Large' | 'Full';

export interface Props extends FooterProps {
    /** Whether the modal is open or not */
    open: boolean;
    /** The url that will be loaded as the content of the modal */
    src?: string;
    /** The name of the modal content iframe */
    iFrameName?: string;
    /** The content for the title of the modal (embedded app use accepts string only) */
    title?: string | React.ReactNode;
    /** The content to display inside modal (stand-alone app use only) */
    children?: React.ReactNode;
    /** Inner content of the footer (stand-alone app use only) */
    footer?: React.ReactNode;
    /** Disable animations and open modal instantly (stand-alone app use only) */
    instant?: boolean;
    /** Automatically adds sections to modal (stand-alone app use only) */
    sectioned?: boolean;
    /** Increases the modal width (stand-alone app use only) */
    large?: boolean;
    /** Limits modal height on large sceens with scrolling (stand-alone app use only) */
    limitHeight?: boolean;
    /** Replaces modal content with a spinner while a background action is being performed (stand-alone app use only) */
    loading?: boolean;
    /**
     * Controls the size of the modal
     * @default 'Small'
     * @embeddedAppOnly
     */
    size?: Size;
    /**
     * Message to display inside modal
     * @embeddedAppOnly
     */
    message?: string;

    /** Callback when the modal is closed */
    onClose(): void;

    /** Callback when iframe has loaded (stand-alone app use only) */
    onIFrameLoad?(evt: React.SyntheticEvent<HTMLIFrameElement>): void;

    /** Callback when modal transition animation has ended (stand-alone app use only) */
    onTransitionEnd?(): void;
}

export type CombinedProps = Props;

export interface State {
    iframeHeight: number;
}

const getUniqueID = createUniqueIDFactory('modal-header');

export class Modal extends React.Component<CombinedProps, State> {
    public static childContextTypes = contentContextTypes;

    public static Dialog = Dialog;
    public static Section = Section;
    public focusReturnPointNode: HTMLElement;

    public state: State = {
        iframeHeight: IFRAME_LOADING_HEIGHT,
    };

    private headerId = getUniqueID();

    public getChildContext() {
        return {
            withinContentContainer: true,
        };
    }

    public componentDidUpdate(prevProps: CombinedProps) {

        const {open} = this.props;
        const wasOpen = prevProps.open;

        if (!wasOpen && open) {
            this.focusReturnPointNode = document.activeElement as HTMLElement;
        } else if (
            wasOpen &&
            !open &&
            this.focusReturnPointNode != null &&
            document.contains(this.focusReturnPointNode)
        ) {
            this.focusReturnPointNode.focus();
            this.focusReturnPointNode = null as any;
        }
    }

    public render() {

        const {
            children,
            title,
            src,
            iFrameName,
            open,
            instant,
            sectioned,
            loading,
            large,
            limitHeight,
            onClose,
            footer,
            primaryAction,
            secondaryActions,
        } = this.props;

        const {iframeHeight} = this.state;

        const iframeTitle = 'Polaris.Modal.iFrameTitle';

        let dialog: React.ReactNode;
        if (open) {
            const footerMarkup =
                !footer && !primaryAction && !secondaryActions ? null : (
                    <Footer
                        primaryAction={primaryAction}
                        secondaryActions={secondaryActions}
                    >
                        {footer}
                    </Footer>
                );

            const content = sectioned
                ? wrapWithComponent(children, Section)
                : children;

            const body = loading ? (
                <div className={styles.Spinner}>
                    <Spinner/>
                </div>
            ) : (
                content
            );

            const bodyMarkup = src ? (
                <iframe
                    name={iFrameName}
                    title={iframeTitle}
                    src={src}
                    className={styles.IFrame}
                    onLoad={this.handleIFrameLoad}
                    style={{height: `${iframeHeight}px`}}
                />
            ) : (
                <Scrollable shadow={true} className={styles.Body}>
                    {body}
                </Scrollable>
            );

            const headerMarkup = title ? (
                <Header id={this.headerId} onClose={onClose}>
                    {title}
                </Header>
            ) : (
                <CloseButton
                    onClick={onClose}
                    title={false}
                />
            );

            dialog = (
                <Dialog
                    instant={instant}
                    labelledBy={this.headerId}
                    onClose={onClose}
                    onEntered={this.handleEntered}
                    onExited={this.handleExited}
                    large={large}
                    limitHeight={limitHeight}
                >
                    {headerMarkup}
                    <div className={styles.BodyWrapper}>{bodyMarkup}</div>
                    {footerMarkup}
                </Dialog>
            );
        }

        const animated = !instant;

        return (
            <Portal idPrefix="modal">
                <TransitionGroup appear={animated} enter={animated} exit={animated}>
                    {dialog}
                </TransitionGroup>
            </Portal>
        );
    }

    @autobind
    private handleEntered() {
        const {onTransitionEnd} = this.props;
        if (onTransitionEnd) {
            onTransitionEnd();
        }
    }

    @autobind
    private handleExited() {
        this.setState({
            iframeHeight: IFRAME_LOADING_HEIGHT,
        });

        if (this.focusReturnPointNode) {
            write(() => focusFirstFocusableNode(this.focusReturnPointNode, false));
        }
    }

    @autobind
    private handleIFrameLoad(evt: React.SyntheticEvent<HTMLIFrameElement>) {
        const iframe = evt.target as HTMLIFrameElement;
        if (iframe && iframe.contentWindow) {
            this.setState({
                iframeHeight: iframe.contentWindow.document.body.scrollHeight,
            });
        }

        const {onIFrameLoad} = this.props;

        if (onIFrameLoad != null) {
            onIFrameLoad(evt);
        }
    }
}


export default Modal;