import { autobind } from '@shopify/javascript-utilities/decorators';
import classNames from "classnames";
import * as React from 'react';
import { CSSTransition } from "react-transition-group";
import { navigationBarCollapsed, ribbonContainerCollapsed } from "../../utilities/breakpoints";
import Button from "../Button/Button";
import EventListener from '../EventListener';
import Icon from "../Icon/Icon";
import { dataPolarisTopBar, Duration, layer } from "../shared";
import TrapFocus from "../TrapFocus";
import styles from './Frame.scss';
import {
    FrameContext,
    frameContextTypes,
} from './types';

export interface Props {
    /** Accepts a top bar component that will be rendered at the top-most portion of an application frame */
    topBar?: React.ReactNode;
    /** Accepts a navigation component that will be rendered in the left sidebar of an application frame */
    navigation?: React.ReactNode;
    /** A boolean property indicating whether the mobile navigation is currently visible
     * @default false
     */
    showMobileNavigation?: boolean;

    showRibbonContainer?: boolean;

    /** Accepts a global ribbon component that will be rendered fixed to the bottom of an application frame */
    globalRibbon?: React.ReactNode;

    /** A callback function to handle clicking the mobile navigation dismiss button */
    onNavigationDismiss?(): void;
}

export interface State {
    mobileView?: boolean;
    skipFocused?: boolean;
    loadingStack: number;
    ribbonView?: boolean;
    showContextualSaveBar: boolean;
}


export const GLOBAL_RIBBON_CUSTOM_PROPERTY = '--global-ribbon-height';
export const APP_FRAME_MAIN = 'AppFrameMain';
const APP_FRAME_NAV = 'AppFrameNav';
const APP_FRAME_TOP_BAR = 'AppFrameTopBar';
const APP_FRAME_LOADING_BAR = 'AppFrameLoadingBar';

export default class Frame extends React.PureComponent<Props, State> {

    public static childContextTypes = frameContextTypes;

    public state: State = {
        skipFocused: false,
        loadingStack: 0,
        mobileView: isMobileView(),
        ribbonView: isRibbonView(),
        showContextualSaveBar: false,
    };

    // private contextualSaveBar: ContextualSaveBarProps | null;

    public getChildContext(): FrameContext {
        return {
            frame: {
                startLoading: this.startLoading,
                stopLoading: this.stopLoading
            },
        };
    }

    public componentDidMount() {
        this.handleResize();
    }

    public render() {
        const {
            skipFocused,
            loadingStack,
            /* toastMessages,*/
            showContextualSaveBar,
            mobileView,
            ribbonView
        } = this.state;
        const {
            children,
            navigation,
            topBar,
            globalRibbon,
            showMobileNavigation = false
        } = this.props;

        const navClassName = classNames(
            styles.Navigation,
            showMobileNavigation && styles['Navigation-visible'],
        );

        const mobileNavHidden = mobileView && !showMobileNavigation;
        const mobileNavShowing = mobileView && showMobileNavigation;
        const tabIndex = mobileNavShowing ? 0 : -1;

        const navigationMarkup = navigation ? (
            <TrapFocus trapping={mobileNavShowing}>
                <CSSTransition
                    appear={mobileView}
                    exit={mobileView}
                    in={showMobileNavigation}
                    timeout={Duration.Base}
                    classNames={navTransitionClasses}
                >
                    <div
                        className={navClassName}
                        onKeyDown={this.handleNavKeydown}
                        id={APP_FRAME_NAV}
                        key="NavContent"
                        hidden={mobileNavHidden}
                    >
                        {navigation}
                        <button
                            type="button"
                            className={styles.NavigationDismiss}
                            onClick={this.handleNavigationDismiss}
                            aria-hidden={
                                mobileNavHidden || (!mobileView && !showMobileNavigation)
                            }
                            aria-label={
                                'Polaris.Frame.Navigation.closeMobileNavigationLabel'
                            }
                            tabIndex={tabIndex}
                        >
                            <Icon source="cancel" color="white"/>
                        </button>
                    </div>
                </CSSTransition>
            </TrapFocus>
        ) : null;

        const loadingMarkup =
            loadingStack > 0 ? (
                <div className={styles.LoadingBar} id={APP_FRAME_LOADING_BAR}>
                    {/*  <Loading />*/}
                </div>
            ) : null;

        const contextualSaveBarMarkup = (
            <CSSTransition
                appear={true}
                exit={true}
                in={showContextualSaveBar}
                timeout={300}
                classNames={contextualSaveBarTransitionClasses}
                mountOnEnter={true}
                unmountOnExit={true}
            >
                <div className={styles.ContextualSaveBar}>
                    {/* <ContextualSaveBar {...this.contextualSaveBar} />*/}
                </div>
            </CSSTransition>
        );

        const topBarMarkup = topBar ? (
            <div
                className={styles.TopBar}
                {...layer.props}
                {...dataPolarisTopBar.props}
                id={APP_FRAME_TOP_BAR}
            >
                {topBar}
            </div>
        ) : null;


        const ribbonClassName = classNames(
            styles.GlobalRibbonContainer,
            ribbonView && styles['GlobalRibbonContainer-hidden'],
        );


        const ribbonMarkup = globalRibbon ? (
            <TrapFocus trapping={ribbonView}>
                <div
                    className={ribbonClassName}
                    key="RibbonContent"
                    hidden={ribbonView}
                >
                    {globalRibbon}
                </div>
            </TrapFocus>
        ) : null;

        const skipClassName = classNames(
            styles.Skip,
            skipFocused && styles.focused,
        );

        const skipMarkup = (
            <div className={skipClassName}>
                <Button
                    onClick={this.handleClick}
                    onFocus={this.handleFocus}
                    onBlur={this.handleBlur}
                >
                    {'Polaris.Frame.skipToContent'}
                </Button>
            </div>
        );

        const navigationAttributes = navigation
            ? {
                'data-has-navigation': true,
            }
            : {};

        const frameClassName = classNames(
            styles.Frame,
            navigation && styles.hasNav,
            topBar && styles.hasTopBar,
        );

        const navigationOverlayMarkup =
            showMobileNavigation && mobileView ? (
                {
                    /* <Backdrop
                                        belowNavigation={true}
                                        onClick={this.handleNavigationDismiss}
                                        onTouchStart={this.handleNavigationDismiss}
                                    />*/
                }
            ) : null;

        return (
            <div
                className={frameClassName}
                {...layer.props}
                {...navigationAttributes}
            >
                {skipMarkup}
                {topBarMarkup}
                {contextualSaveBarMarkup}
                {loadingMarkup}
                {navigationOverlayMarkup}
                {navigationMarkup}
                <main
                    className={styles.Main}
                    id={APP_FRAME_MAIN}
                    data-has-ribbon={Boolean(globalRibbon)}
                >
                    <div className={styles.Content}>{children}</div>
                </main>
                {ribbonMarkup}
                <EventListener event="resize" handler={this.handleResize}/>
            </div>
        );
    }

    @autobind
    private startLoading() {
        this.setState(({loadingStack}: State) => ({
            loadingStack: loadingStack + 1,
        }));
    }

    @autobind
    private stopLoading() {
        this.setState(({loadingStack}: State) => ({
            loadingStack: Math.max(0, loadingStack - 1),
        }));
    }

    @autobind
    private handleResize() {
        const {mobileView, ribbonView} = this.state;

        if (isMobileView() && !mobileView) {
            this.setState({mobileView: true});
        } else if (!isMobileView() && mobileView) {
            this.setState({mobileView: false});
        }

        if (isRibbonView() && !ribbonView) {
            this.setState({ribbonView: true});
        } else if (!isRibbonView() && ribbonView) {
            this.setState({ribbonView: false});
        }
    }

    @autobind
    private handleClick() {
        focusAppFrameMain();
    }

    @autobind
    private handleFocus() {
        this.setState({skipFocused: true});
    }

    @autobind
    private handleBlur() {
        this.setState({skipFocused: false});
    }

    @autobind
    private handleNavigationDismiss() {
        const {onNavigationDismiss} = this.props;
        if (onNavigationDismiss != null) {
            onNavigationDismiss();
        }
    }

    @autobind
    private handleNavKeydown(event: React.KeyboardEvent<HTMLElement>) {
        const {key} = event;

        if (key === 'Escape') {
            this.handleNavigationDismiss();
        }
    }
}

const navTransitionClasses = {
    enter: classNames(styles['Navigation-enter']),
    enterActive: classNames(styles['Navigation-enterActive']),
    enterDone: classNames(styles['Navigation-enterActive']),
    exit: classNames(styles['Navigation-exit']),
    exitActive: classNames(styles['Navigation-exitActive']),
};

const contextualSaveBarTransitionClasses = {
    enter: classNames(styles['ContextualSaveBar-enter']),
    enterActive: classNames(styles['ContextualSaveBar-enterActive']),
    enterDone: classNames(styles['ContextualSaveBar-enterActive']),
    exit: classNames(styles['ContextualSaveBar-exit']),
    exitActive: classNames(styles['ContextualSaveBar-exitActive']),
};

function focusAppFrameMain() {
    window.location.assign(`${window.location.pathname}#${APP_FRAME_MAIN}`);
}

function isMobileView() {
    return navigationBarCollapsed().matches;
}

function isRibbonView() {
    return ribbonContainerCollapsed().matches;
}