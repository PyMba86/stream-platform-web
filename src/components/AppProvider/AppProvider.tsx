import * as React from 'react';

export default class AppProvider extends React.Component {
    private subscriptions: Array<() => void> = [];

    public componentWillReceiveProps() {
        this.subscriptions.forEach((subscriberCallback) => subscriberCallback());
    }

    public render() {
        return (
            React.Children.only(this.props.children)
        );
    }

    public subscribe(callback: () => void) {
        this.subscriptions.push(callback);
    }

    public unsubscribe(callback: () => void) {
        this.subscriptions = this.subscriptions.filter(
            (subscription) => subscription !== callback,
        );
    }

}