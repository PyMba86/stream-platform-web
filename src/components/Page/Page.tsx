import { classNames } from '@shopify/react-utilities/styles';
import * as React from 'react';

import { Header, HeaderProps } from './components';
import { List } from "./components";
import styles from './Page.scss';

export interface Props extends HeaderProps {
    /** The contents of the page */
    children?: React.ReactNode;
    /** Remove the normal max-width on the page */
    fullWidth?: boolean;
    /** Decreases the maximum layout width. Intended for single-column layouts */
    singleColumn?: boolean;
}

export type ComposedProps = Props;

export class Page extends React.PureComponent<ComposedProps, never> {

    public static List = List;

    public render() {
        const {children, fullWidth, singleColumn, ...rest} = this.props;

        const className = classNames(
            styles.Page,
            fullWidth && styles.fullWidth,
            singleColumn && styles.singleColumn,
        );

        const headerMarkup = <Header {...rest} />;

        return (
            <div className={className}>
                {headerMarkup}
                <div className={styles.Content}>{children}</div>
            </div>
        );
    }
}

export default Page;