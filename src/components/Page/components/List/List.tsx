import { createUniqueIDFactory } from '@shopify/javascript-utilities/other';
import * as React from 'react';
import styles from './List.scss';

export interface Item {
    /**  Item content */
    page: React.ReactNode;
}

export interface Props {
    /** Collection of items for list */
    pages: Item[];
}

const getUniquePageKey = createUniqueIDFactory(`Page`);

export default function List({pages}: Props) {
    const pagesWrapper = pages.reduce(
        (allPages, {page}) => [
            ...allPages,
            <div key={getUniquePageKey()} className={styles.PageListItem}>
                {page}
            </div>,
        ],
        [] as React.ReactNode[],
    );

    return <div className={styles.PageList}>{pagesWrapper}</div>;
}