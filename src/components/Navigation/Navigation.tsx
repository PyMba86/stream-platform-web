import * as React from 'react';

import Scrollable from '../Scrollable';

import { Item, Section } from './components';
import { contextTypes, SectionType } from './types';

import styles from './Navigation.scss';

export interface Props {
    location: string;
    sections?: SectionType[];
    children?: React.ReactNode;

    onDismiss?(): void;
}

export default class Navigation extends React.Component<Props, never> {
    public static Item = Item;
    public static Section = Section;
    public static childContextTypes = contextTypes;

    public getChildContext() {
        return {
            location: this.props.location,
            onNavigationDismiss: this.props.onDismiss,
        };
    }

    public render() {
        const {children} = this.props;

        return (
            <nav className={styles.Navigation}>
                <Scrollable className={styles.PrimaryNavigation}>{children}</Scrollable>
            </nav>
        );
    }
}