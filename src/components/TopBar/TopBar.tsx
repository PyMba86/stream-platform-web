import { autobind } from '@shopify/javascript-utilities/decorators';
import { classNames } from '@shopify/react-utilities/styles';
import * as React from 'react';

import { menu } from '../../icons';
import Icon from '../Icon';

import { Menu, Search, SearchField, SearchProps, UserMenu } from './components';
import styles from './TopBar.scss';

export interface Props {
    /** Toggles whether or not a navigation component has been provided. Controls the presence of the mobile nav toggle button */
    showNavigationToggle?: boolean;
    /** Accepts a user component that is made available as a static member of the top bar component and renders as the primary menu */
    userMenu?: React.ReactNode;
    /** Accepts a user component that is made available as a static member of the top bar component and renders as the primary menu */
    logoMenu?: React.ReactNode;
    /** Accepts a menu component that is made available as a static member of the top bar component */
    secondaryMenu?: React.ReactNode;
    /** Accepts a search field component that is made available as a `TextField` static member of the top bar component */
    searchField?: React.ReactNode;
    /** Accepts a search results component that is ideally composed of a card component containing a list of actionable search results */
    searchResults?: React.ReactNode;
    /** A boolean property indicating whether search results are currently visible. */
    searchResultsVisible?: boolean;
    /** A callback function that handles the dismissal of search results */
    onSearchResultsDismiss?: SearchProps['onDismiss'];

    /** A callback function that handles hiding and showing mobile navigation */
    onNavigationToggle?(): void;
}

export type ComposedProps = Props;

export interface State {
    focused: boolean;
}

export class TopBar extends React.PureComponent<ComposedProps, State> {
    public static UserMenu = UserMenu;
    public static SearchField = SearchField;
    public static Menu = Menu;

    public state: State = {
        focused: false,
    };

    public render() {
        const {
            showNavigationToggle,
            userMenu,
            logoMenu,
            searchResults,
            searchField,
            secondaryMenu,
            searchResultsVisible,
            onNavigationToggle,
            onSearchResultsDismiss
        } = this.props;

        const {focused} = this.state;

        const className = classNames(
            styles.NavigationIcon,
            focused && styles.focused,
        );

        const navigationButtonMarkup = showNavigationToggle ? (
            <button
                type="button"
                className={className}
                onClick={onNavigationToggle}
                onFocus={this.handleFocus}
                onBlur={this.handleBlur}
                aria-label="Toggle menu"
            >
                <Icon source={menu} color="black"/>
            </button>
        ) : null;


        const searchResultsMarkup =
            searchResults && searchResultsVisible ? (
                <Search
                    visible={searchResultsVisible}
                    onDismiss={onSearchResultsDismiss}
                >
                    {searchResults}
                </Search>
            ) : null;

        const searchMarkup = searchField ? (
            <React.Fragment>
                {searchField}
                {searchResultsMarkup}
            </React.Fragment>
        ) : null;

        return (
            <div className={styles.TopBar}>
                {navigationButtonMarkup}
                <div className={styles.LogoContainer}>
                    {logoMenu}
                </div>
                <div className={styles.Contents}>
                    {searchMarkup}
                    {secondaryMenu}
                    {userMenu}
                </div>
            </div>
        );
    }

    @autobind
    private handleFocus() {
        this.setState({focused: true});
    }

    @autobind
    private handleBlur() {
        this.setState({focused: false});
    }
}

export default TopBar;