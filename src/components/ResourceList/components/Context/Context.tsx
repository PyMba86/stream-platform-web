import * as React from 'react';
import { ResourceListContext } from '../../types';

const {Provider, Consumer} = React.createContext<ResourceListContext>({
    selectMode: false,
    resourceName: {
        singular: 'Polaris.ResourceList.defaultItemSingular',
        plural: 'Polaris.ResourceList.defaultItemPlural',
    },
});

export { Provider, Consumer };