import { focusFirstFocusableNode } from '@shopify/javascript-utilities/focus';
import isEqual from 'lodash/isEqual';
import * as React from 'react';
import * as ReactDOM from 'react-dom';

export interface Props {
    children?: React.ReactNode;
    disabled?: boolean;
}

export default class Focus extends React.PureComponent<Props, never> {
    public componentDidMount() {
        this.handleSelfFocus();
    }

    public componentDidUpdate({children: prevChildren, ...restPrevProps}: Props) {
        const {children, ...restProps} = this.props;

        if (isEqual(restProps, restPrevProps)) {
            return;
        }

        this.handleSelfFocus();
    }

    public handleSelfFocus() {
        if (this.props.disabled) {
            return;
        }

        const root = ReactDOM.findDOMNode(this) as HTMLElement | null;
        if (root) {
            if (!root.querySelector('[autofocus]')) {
                focusFirstFocusableNode(root, false);
            }
        }
    }

    public render() {
        const {children} = this.props;

        return <React.Fragment>{children}</React.Fragment>;
    }
}