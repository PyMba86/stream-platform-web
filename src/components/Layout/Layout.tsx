import * as React from 'react';
import { AnnotatedSection, Section } from './components';
import styles from './Layout.scss';

export interface Props {
    /** Automatically adds sections to layout. */
    sectioned?: boolean;
    /** The content to display inside the layout. */
    children?: React.ReactNode;
}

export default class Layout extends React.Component<Props, never> {
    public static AnnotatedSection = AnnotatedSection;
    public static Section = Section;

    public render() {
        const {children, sectioned} = this.props;

        const content = sectioned ? <Section>{children}</Section> : children;

        return <div className={styles.Layout}>{content}</div>;
    }
}