import * as React from 'react';

import DisplayText from '../DisplayText';
import Stack from '../Stack';
import TextStyle from '../TextStyle';

export interface Props {
    title: string;
    description?: string;
    withIllustration?: boolean;
}

export type CombinedProps = Props ;

export class EmptySearchResult extends React.PureComponent<
    CombinedProps,
    never
    > {
    public render() {
        const {
            title,
            description
        } = this.props;

        const descriptionMarkup = description ? <p>{description}</p> : null;
        return (
            <Stack alignment="center" vertical={true}>
                <DisplayText size="small">{title}</DisplayText>
                <TextStyle variation="subdued">{descriptionMarkup}</TextStyle>
            </Stack>
        );
    }
}

export default EmptySearchResult;