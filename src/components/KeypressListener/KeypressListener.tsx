import { autobind } from '@shopify/javascript-utilities/decorators';
import {
    addEventListener,
    removeEventListener,
} from '@shopify/javascript-utilities/events';
import * as React from 'react';
import { Key } from '../../types';

export interface Props {
    keyCode: Key;

    handler(event: KeyboardEvent): void;
}

export default class KeypressListener extends React.Component<Props, never> {
    public componentDidMount() {
        addEventListener(document, 'keyup', this.handleKeyEvent);
    }

    public componentWillUnmount() {
        removeEventListener(document, 'keyup', this.handleKeyEvent);
    }

    public render() {
        return null;
    }

    @autobind
    private handleKeyEvent(event: KeyboardEvent) {
        const {keyCode, handler} = this.props;

        if (event.keyCode === keyCode) {
            handler(event);
        }
    }
}