import * as React from 'react';
import { FrameContext, frameContextTypes } from '../Frame';

export interface Props {
}

export type ComposedProps = Props;

export class Loading extends React.PureComponent<ComposedProps, never> {
    public static contextTypes = frameContextTypes;
    public context: FrameContext;

    public componentDidMount() {
        this.context.frame.startLoading();
    }

    public componentWillUnmount() {
        this.context.frame.stopLoading();
    }

    public render() {
        return null;
    }
}
