import { RouterState, RouterStore } from "mobx-state-router";

const checkContainsProfileKey = (fromState: RouterState, toState: RouterState, routerStore: RouterStore) => {
    const {
        rootStore: {profileStore}
    } = routerStore;
    if (profileStore.checkAuth()) {
        return Promise.resolve();
    } else {
        return Promise.reject(new RouterState('login'));
    }
};

const checkContainsProfileCookieKey = (fromState: RouterState, toState: RouterState, routerStore: RouterStore) => {
    const {
        rootStore: {profileStore}
    } = routerStore;
    if (!profileStore.checkAuth()) {
        return Promise.resolve();
    } else {
        return Promise.reject(new RouterState('managers'));
    }
};

export const routes = [
    {name: 'login', pattern: '/login', beforeEnter: checkContainsProfileCookieKey},
    {name: 'signup', pattern: '/signup', beforeEnter: checkContainsProfileCookieKey},
    {name: 'managers', pattern: '/', beforeEnter: checkContainsProfileKey},
    {name: 'notFound', pattern: '/not-found'},
];