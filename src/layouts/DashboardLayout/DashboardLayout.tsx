import { RouterStore } from 'mobx-state-router';
import React from 'react';
import { inject, observer } from 'ts-mobx-react';
import AppProvider from "../../components/AppProvider/AppProvider";
import Frame from "../../components/Frame";
import { Loading } from "../../components/Loading/Loading";
import TopBar from "../../components/TopBar";
import ProfileStore from "../../store/ProfileStore";


@observer
export class DashboardLayout extends React.Component {

    @inject('routerStore')
    public routerStore: RouterStore;

    @inject('profileStore')
    public profileStore: ProfileStore;

    public state = {
        isLoading: false,
        isDirty: false,
        searchActive: false,
        userMenuOpen: false,
        logoMenuOpen: false,
        showMobileNavigation: false,
        modalActive: false,
        supportSubject: '',
        supportMessage: '',
    };

    public render() {
        const {
            isLoading,
            searchActive,
            userMenuOpen,
            showMobileNavigation
        } = this.state;

        const {
            children
        } = this.props;

        const userMenuActions = [
            {
                items: [{
                    content: 'Выйти', onAction: () => {
                        this.profileStore.logout().then(resolve => {
                            this.routerStore.goTo("login");
                        });
                    }
                }],
            },
        ];

        const userMenuMarkup = (
            <TopBar.UserMenu
                actions={userMenuActions}
                name= {this.profileStore.profile}
                detail="Базовая"
                initials={this.profileStore.profile.charAt(0).toUpperCase()}
                open={userMenuOpen}
                onToggle={this.toggleState('userMenuOpen')}
            />
        );

        const topBarMarkup = (
            <TopBar
                showNavigationToggle={true}
                userMenu={userMenuMarkup}
                searchResultsVisible={searchActive}
                onSearchResultsDismiss={this.handleSearchResultsDismiss}
                onNavigationToggle={this.toggleState('showMobileNavigation')}
            />
        );

        const loadingMarkup = isLoading ? <Loading/> : null;

        return (
            <AppProvider>
                <Frame
                    topBar={topBarMarkup}
                    showMobileNavigation={showMobileNavigation}
                    onNavigationDismiss={this.toggleState('showMobileNavigation')}
                >
                    {loadingMarkup}
                    {children}
                </Frame>
            </AppProvider>
        );
    }

    public toggleState = (key: string) => {
        return () => {
            this.setState((prevState) => ({[key]: !prevState[key]}));
        };
    };

    public handleSearchFieldChange = (value: string) => {
        this.setState({searchText: value});
        if (value.length > 0) {
            this.setState({searchActive: true});
        } else {
            this.setState({searchActive: false});
        }
    };


    public handleSearchResultsDismiss = () => {
        this.setState(() => {
            return {
                searchActive: false,
                searchText: '',
            };
        });
    };

}
